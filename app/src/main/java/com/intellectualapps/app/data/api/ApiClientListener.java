package com.intellectualapps.app.data.api;

import com.intellectualapps.app.data.api.responses.DefaultResponse;

public class ApiClientListener {
    public interface AccountRegistrationListener {
        void onAccountRegistered(DefaultResponse defaultResponse);
    }
}
