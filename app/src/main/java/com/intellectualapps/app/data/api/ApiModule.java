package com.intellectualapps.app.data.api;


import com.intellectualapps.app.BuildConfig;

import java.io.File;
import java.io.IOException;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.Cache;
import okhttp3.Dispatcher;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiModule {
    private File cacheFile;
    private static ApiService apiService;
    private Map<String, String> headers;
    private static ApiModule apiModuleInstance;
    private static Retrofit.Builder retrofitBuilder;
    private static OkHttpClient.Builder okHttpClientBuilder;
    private static Retrofit retrofit;
    private static Dispatcher mDispatcher;

    public ApiModule(File cacheFile, final Map<String, String> headers, Boolean flag) {
        this.cacheFile = cacheFile;
        this.headers = headers;
        apiService = getApiModuleInstance(cacheFile, headers).getApiService();
        apiModuleInstance = this;
    }

    private ApiModule(File cacheFile, final Map<String, String> headers) {
        this.cacheFile = cacheFile;
        this.headers = headers;
        retrofit = buildRetrofitAdapter(cacheFile, headers);
        apiService = retrofit.create(ApiService.class);
        apiModuleInstance = this;
    }

    public static void resetApiClient() {
        retrofit = null;
        retrofitBuilder = null;
        okHttpClientBuilder = null;
        apiModuleInstance = null;
        apiService = null;
    }

    ApiService getApiService() {
        return apiService;
    }


    static ApiModule getApiModuleInstance(File cacheFile, final Map<String, String> headers) {
        if (apiModuleInstance == null) {
            apiModuleInstance = new ApiModule(cacheFile, headers);
        }

        return apiModuleInstance;
    }

    private static OkHttpClient buildOkHttpClient(File cacheFile, final Map<String, String> headers) {
        if (okHttpClientBuilder == null) {
            okHttpClientBuilder = new OkHttpClient.Builder();

            Cache cache = null;
            try {
                cache = new Cache(cacheFile, 10 * 1024 * 1024);
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (BuildConfig.DEBUG) {
                HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
                logging.setLevel(HttpLoggingInterceptor.Level.BODY);
                okHttpClientBuilder.addInterceptor(logging);
            }

            okHttpClientBuilder.connectTimeout(60 * 2000, TimeUnit.MILLISECONDS)
                    .readTimeout(60 * 2000, TimeUnit.MILLISECONDS)
                    .writeTimeout(20 * 5000, TimeUnit.MILLISECONDS);

            mDispatcher = new Dispatcher();
            //mDispatcher.setMaxRequestsPerHost(1);
            //mDispatcher.setMaxRequests(1);

            okHttpClientBuilder.addInterceptor(new Interceptor() {
                @Override
                public Response intercept(Chain chain) throws IOException {
                    Request original = chain.request();
                    Request.Builder requestBuilder = original.newBuilder();
                    requestBuilder.header("Content-Type", "application/json")
                            .removeHeader("Pragma")
                            .header("Cache-Control", String.format(Locale.getDefault(), "max-age=%d", 432000))
                            .build();
                    if (headers != null && headers.size() > 0) {
                        for (String key : headers.keySet()) {
                            if (headers.get(key) != null) {
                                requestBuilder.header(key, headers.get(key));
                            }
                        }
                    }
                    Request request = requestBuilder.build();
                    Response response = chain.proceed(request);
                    response.cacheResponse();
                    return response;
                }
            }).dispatcher(mDispatcher);
            okHttpClientBuilder.cache(cache);
        }
        return okHttpClientBuilder.build();
    }

    public static void cancelAllRequests() {
        if (mDispatcher != null) {
            mDispatcher.executorService().shutdown();
        }
    }

    private static Retrofit.Builder buildRetrofitBuilder(OkHttpClient okHttpClient) {
        if (retrofitBuilder == null) {
            retrofitBuilder = new Retrofit.Builder()
                    .client(okHttpClient)
                    .baseUrl("http://104.154.208.252:8080/pw/api/v1/")
                    .addConverterFactory(GsonConverterFactory.create());
        }

        return retrofitBuilder;
    }

    public static Retrofit buildRetrofitAdapter(File cacheFile, Map<String, String> headers) {
        if (retrofit == null) {
            retrofit = buildRetrofitBuilder(buildOkHttpClient(cacheFile, headers)).build();
        }

        return retrofit;
    }
}
