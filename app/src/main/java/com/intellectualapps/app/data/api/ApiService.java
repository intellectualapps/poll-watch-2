package com.intellectualapps.app.data.api;

import com.intellectualapps.app.data.api.responses.AuthenticationResponse;
import com.intellectualapps.app.data.api.responses.DefaultResponse;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.POST;
import retrofit2.http.QueryMap;


interface ApiService {
    @POST("user/authenticate")
    Call<AuthenticationResponse> authenticateUser(@QueryMap Map<String, String> dataMap);
}
