package com.intellectualapps.app.data.api.responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.intellectualapps.app.data.models.ElectionEvent;
import com.intellectualapps.app.data.models.User;

import java.util.List;

public class AuthenticationResponse extends DefaultResponse {
    @SerializedName("electionEvents")
    @Expose
    private List<ElectionEvent> electionEvents;
    @SerializedName("user")
    @Expose
    private User user;

    public List<ElectionEvent> getElectionEvents() {
        return electionEvents;
    }

    public void setElectionEvents(List<ElectionEvent> electionEvents) {
        this.electionEvents = electionEvents;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
