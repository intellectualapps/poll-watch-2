package com.intellectualapps.app.ui.activities;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;

import com.intellectualapps.app.R;
import com.intellectualapps.app.data.models.ElectionEvent;
import com.intellectualapps.app.databinding.ActivityActiveElectionEventsBinding;
import com.intellectualapps.app.ui.adapters.ActiveElectionAdapter;
import com.intellectualapps.app.utils.PreferenceStorageManager;

import java.util.ArrayList;
import java.util.List;

public class ActiveElectionEventsActivity extends BaseActivity {
    ActivityActiveElectionEventsBinding binding;
    private List<ElectionEvent> electionsList = new ArrayList<ElectionEvent>();
    private ActiveElectionAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_active_election_events);
        electionsList = PreferenceStorageManager.getElectionEvents(getApplicationContext());
        adapter = new ActiveElectionAdapter(electionsList);
        binding.recyclerView.setAdapter(adapter);
    }

    public void onClick(View v) {
        Intent intent = new Intent(this, LaunchActivity.class);
    }

    public void logout(View view){
        Intent intent=new Intent(this,VotersAndVotesActivity.class);
        startActivity(intent);
    }

    public void fillData(){
        electionsList.add(new ElectionEvent("Presidential Election", "25th July 2018"));
        electionsList.add(new ElectionEvent("Kano Guberanatorial Election", "25th june 2018"));
        electionsList.add(new ElectionEvent("Governorship Election", "26th October 2018"));
    }

    public ActiveElectionEventsActivity() {
    }

    public static ActiveElectionEventsActivity newInstance() {
        return new ActiveElectionEventsActivity();
    }
}
