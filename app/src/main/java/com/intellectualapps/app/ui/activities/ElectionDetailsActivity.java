package com.intellectualapps.app.ui.activities;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;


import com.intellectualapps.app.R;
import com.intellectualapps.app.data.models.ElectionDetails;
import com.intellectualapps.app.databinding.ActivityElectionDetailsBinding;
import com.intellectualapps.app.ui.adapters.ElectionDetailsAdapter;

import java.util.ArrayList;


public class ElectionDetailsActivity extends AppCompatActivity {
    ActivityElectionDetailsBinding binding;
    private ArrayList<ElectionDetails> electionsDetails = new ArrayList<ElectionDetails>();
    private ElectionDetailsAdapter adapter;
    private EditText editText;
    private Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_election_details);
        fillData();
        adapter = new ElectionDetailsAdapter(electionsDetails);
        binding.electionRecyclerView.setAdapter(adapter);
        String[] arraySpinner = new String[] {
                "Party Name", "APC Party", "PDP Party "
        };
        Spinner s = (Spinner) findViewById(R.id.spinner);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, arraySpinner);
        adapter.setDropDownViewResource(android.R.layout.simple_list_item_1);
        s.setAdapter(adapter);
    }

    public void fillData(){
        electionsDetails.add(new ElectionDetails("APC", 4123));
        electionsDetails.add(new ElectionDetails("APGA", 1321));
        electionsDetails.add(new ElectionDetails("Labour", 321));
    }

    public void nextScreen(View view) {
        Intent intent=new Intent(this, ResultSheetPhotoActivity.class);
        startActivity(intent);
    }

    public void previous(View view) {
        Intent intent=new Intent(this, VotersAndVotesActivity.class);
        startActivity(intent);
    }

    public static ElectionDetailsActivity newInstance() {
        return new ElectionDetailsActivity();
    }

    public void ResultSheet(View v) {
        Intent intent = new Intent(this, ResultSheetPhotoActivity.class);
        startActivity(intent);
    }

    public void AddCount(View view) {
    }
}


