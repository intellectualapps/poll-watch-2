package com.intellectualapps.app.ui.activities;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;

import com.intellectualapps.app.R;
import com.intellectualapps.app.data.models.ElectionEvent;
import com.intellectualapps.app.databinding.ActivityFilledFormsBinding;
import com.intellectualapps.app.ui.adapters.FilledFormsAdapter;

import java.util.ArrayList;

public class FilledFormsActivity extends AppCompatActivity {
    Context context;
    ActivityFilledFormsBinding binding;
    private ArrayList<ElectionEvent> electionsList = new ArrayList<ElectionEvent>();
    private FilledFormsAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_filled_forms);
        fillData();
        adapter = new FilledFormsAdapter(electionsList);
        binding.recyclerView.setAdapter(adapter);
    }

    public void fillData(){
        electionsList.add(new ElectionEvent("Presidential Election", "25th July 2018"));
    }

    public FilledFormsActivity() {
    }

    public void close(View view){
        Intent intent=new Intent(this, ActiveElectionEventsActivity.class);
        startActivity(intent);
    }

    public static FilledFormsActivity newInstance() {
        return new FilledFormsActivity();
    }
}
