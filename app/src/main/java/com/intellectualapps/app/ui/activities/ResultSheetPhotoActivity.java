package com.intellectualapps.app.ui.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;

import com.intellectualapps.app.R;

public class ResultSheetPhotoActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_result_sheet_photo);
    }

    public void vote(View v){
        Intent intent=new Intent(this, VotersAndVotesActivity.class);
        startActivity(intent);
    }

    public void nextScreen(View view) {
        Intent intent=new Intent(this, ElectionPhotoActivity.class);
        startActivity(intent);
    }

    public void previous(View view) {
        Intent intent=new Intent(this, ElectionDetailsActivity.class);
        startActivity(intent);
    }
}
