package com.intellectualapps.app.ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.intellectualapps.app.R;
import com.intellectualapps.app.data.api.ApiModule;
import com.intellectualapps.app.data.api.HttpService;
import com.intellectualapps.app.data.api.responses.AuthenticationResponse;
import com.intellectualapps.app.data.models.ElectionEvent;
import com.intellectualapps.app.data.models.User;
import com.intellectualapps.app.ui.activities.BaseActivity;
import com.intellectualapps.app.utils.ApiUtils;
import com.intellectualapps.app.utils.CommonUtils;
import com.intellectualapps.app.utils.Constants;
import com.intellectualapps.app.utils.NetworkUtils;
import com.intellectualapps.app.utils.PreferenceStorageManager;
import com.intellectualapps.app.utils.SharedPrefsUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginFragment extends BaseFragment implements View.OnClickListener {
    private User user;
    private TextView resetPasswordButton;
    private EditText usernameInput, passwordInput;
    private Button loginButton;
    private Toolbar toolbar;


    public static Fragment newInstance(Bundle args) {
        Fragment frag = new LoginFragment();
        if (args == null) {
            args = new Bundle();
        }
        frag.setArguments(args);
        return frag;
    }

    public LoginFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login, container, false);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        mSnackBarView = view;
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view, savedInstanceState);
    }

    private void init(View view, Bundle savedInstanceState) {
        ((BaseActivity) getActivity()).getToolbar().setVisibility(View.GONE);
        usernameInput = (EditText) view.findViewById(R.id.username);
        passwordInput = (EditText) view.findViewById(R.id.password);
        loginButton = (Button) view.findViewById(R.id.login_button);

        loginButton.setOnClickListener(this);
        loadSavedLoginData();
    }

    @Override
    public void onClick(View v) {
        String message = getString(R.string.login_progress_label);
        Bundle bundle = new Bundle();

        switch (v.getId()) {
            case R.id.login_button:
                //Intent intent=new Intent(getActivity(), ActiveElectionEventsActivity.class);
                //getActivity().startActivity(intent);
                if (validateTextInput(usernameInput) && validateTextInput(passwordInput)) {

                    if (NetworkUtils.isConnected(getContext())) {
                        hideKeyboard();
                        showLoadingIndicator(message);
                        getHttpService().authenticateUser(getUserInput(), authenticationCallback());
                    } else {
                        CommonUtils.displaySnackBarMessage(mSnackBarView, getString(R.string.network_connection_label));
                    }
                }
                break;
        }
    }

    public Map<String, String> getUserInput() {
        Map<String, String> map = new HashMap<String, String>();
        String password = passwordInput.getText().toString().trim();
        String id = usernameInput.getText().toString().trim();
        map.put(Constants.PASSWORD, password);
        map.put(Constants.ID, id);
        map.put(Constants.AUTH_TYPE, Constants.USERNAME_AUTH_TYPE);
        return map;
    }

    private void loadSavedLoginData() {
        Map<String, String> loginData = new HashMap<String, String>();
        loginData = PreferenceStorageManager.getLoginData(getContext().getApplicationContext());
        if (loginData != null && loginData.size() > 0) {
            usernameInput.setText(loginData.get(Constants.ID));
            passwordInput.setText(loginData.get(Constants.PASSWORD));
        }
    }

    private Callback<AuthenticationResponse> authenticationCallback() {
        return new Callback<AuthenticationResponse>() {
            @Override
            public void onResponse(Call<AuthenticationResponse> call, Response<AuthenticationResponse> response) {
                AuthenticationResponse authenticationResponse = response.body();
                hideLoadingIndicator();
                if (ApiUtils.isSuccessResponse(response)) {
                    if (authenticationResponse != null) {
                        if (authenticationResponse.getStatus() == null && authenticationResponse.getMessage() == null) {
                            HttpService.resetApiService();
                            ApiModule.resetApiClient();
                            user = authenticationResponse.getUser();
                            PreferenceStorageManager.saveUser(getActivity(), user);
                            PreferenceStorageManager.saveElectionEvents(getActivity(), authenticationResponse.getElectionEvents());
                            showMainActivity(user, getActivity());
                        } else {
                            showErrorPopupMessage(authenticationResponse.getMessage());
                        }
                    } else {
                        showErrorPopupMessage(getString(R.string.null_response_label));
                    }
                }
            }

            @Override
            public void onFailure(Call<AuthenticationResponse> call, Throwable t) {
                hideLoadingIndicator();
                showErrorPopupMessage(t.getMessage());
            }
        };
    }
}
