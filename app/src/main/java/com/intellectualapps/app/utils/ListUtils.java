package com.intellectualapps.app.utils;

import java.util.Collection;

public class ListUtils {
    public static boolean isEmpty(Collection items) {
        return items == null || items.size() < 1;
    }
}
